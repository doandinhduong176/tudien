import Manager.DictionaryCommandline;
import Manager.DictionaryManagement;
import World.Dictionary;
import World.Vocabulary;
import World.Word;

import java.awt.Desktop;
import java.io.*;
import java.util.Set;

import java.util.ArrayList;
import java.util.HashSet;

import javafx.fxml.FXML;
import javafx.scene.Cursor;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.SepiaTone;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.GridPane;
import javafx.scene.text.Font;
import javazoom.jl.decoder.JavaLayerException;

import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.List;
import java.util.stream.Collectors;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;

import javax.swing.*;
import java.io.IOException;
import java.net.URL;
import java.util.*;
public class SceneControl {
    final Font DEFAULT_BIG_FONT = Font.font("Arial", 14);
    final Font DEFAULT_SMALL_FONT = Font.font("Arial", 12);
    final Cursor DEFAULT_BUTTON_CURSOR = Cursor.HAND;
    final int SMALL_BUTTON_HEIGHT = 14;
    final int BIG_BUTTON_HEIGHT = 16;
    final String DEFAULT_STYLE = "-fx-background-color: #d98fcd;";
    final public String LEFT = "left";
    final public String RIGHT = "right";
    final float EXPLAIN_BOT_ANCHOR = 75;
    final float EXAMPLE_HEIGHT = 55;

    public Dictionary dictionary = new Dictionary();
    public ArrayList<Vocabulary> diction = dictionary.getDict();
    /**
     *danh sách hiển thị nghĩa của từ
     */
    private TextArea textArea ;
    @FXML
    private TextField searchField,addField_Target,addField_Explain;
    //danh sách từ theo dạng list
    public ListView listView = new ListView();

    /**
     * Chức năng thêm từ
     */
    public void add_word(KeyEvent event)
    {
        if(event.getCode()==KeyCode.SHIFT )
        {
            String english=addField_Target.getText();
            String vn=addField_Explain.getText();
            if(english.length()>0 &&vn.length()>0) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setContentText("Bạn muốn thêm từ này???");
                alert.showAndWait();
                List<String> newWord = DictionaryManagement.push(english, vn);
                DictionaryManagement.dictionaryExportToFile();
                ObservableList<String> data = FXCollections.observableArrayList(newWord);
                listView.setItems(data);
                newWord.clear();
                addField_Explain.clear();
                addField_Target.clear();
            }
            else
            {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Welcome!!!");
                alert.setHeaderText(null);
                alert.setContentText("Xác định từ chính xác.");
                alert.showAndWait();
            }

        }
    }
    public void wordlookup(KeyEvent event)
    {
        if(event.getCode()==KeyCode.ENTER) {
            String wordLook = searchField.getText();
            textArea.setText(DictionaryManagement.dictionaryLookup(wordLook));
        }
    }
    public void deleteword(KeyEvent event) throws IOException { 
        //xóa từ
        try
        {   textArea.setText(DictionaryManagement.dictionaryLookup(listView.getSelectionModel().getSelectedItem().toString()));
            String del=listView.getSelectionModel().getSelectedItem().toString();
            if(event.getCode()==KeyCode.DELETE) {
                Alert alert = new Alert(Alert.AlertType.INFORMATION);
                alert.setHeaderText(null);
                alert.setContentText("Đã xóa từ thành công");
                alert.showAndWait();
                textArea.clear();
                searchField.clear();
                List<String> pull_out = DictionaryManagement.removeWordFromDitionary(del);
                DictionaryManagement.dictionaryExportToFile();
                ObservableList<String> data = FXCollections.observableArrayList(pull_out);
                Collections.sort(data);
                listView.setItems(data);
                pull_out.clear();
            }

        }catch (Exception e)
        {
            System.out.println(">>>");
        }
    }
    //ham đóng ứng dụng
    public void exit(ActionEvent event) {
        Platform.exit();
        System.exit(0);
    }
    //hàm tìm kiếm hiển thị danh sách từ
    public void inputsearch(KeyEvent event)
    {
        String look=searchField.getText().toString();
        List<String> s= DictionaryManagement.Search(look);
        ObservableList<String> input = FXCollections.observableArrayList(s);
        listView.setItems(input);
        listView.scrollTo(look);
        textArea.clear();
        DictionaryManagement.add_up.clear();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        final Tooltip tooltip=new Tooltip();
        tooltip.setText("Bạn có thể thực hiện các thao tác như chỉnh sửa, thêm mới, xóa, thoát");
        menuBar.setTooltip(tooltip);
        final Tooltip tooltip1=new Tooltip();
        tooltip1.setText("Thao tác tìm kiếm từ.");
        searchField.setTooltip(tooltip1);
        final Tooltip tooltip2=new Tooltip();
        tooltip2.setText("Phát âm");
        speaker.setTooltip(tooltip2);
        final Tooltip tooltip3=new Tooltip();
        tooltip3.setText("Bạn có thể xem nghĩa của từ mà mình tìm kiếm ở đây, sủa đổi");
        textArea.setTooltip(tooltip3);
        VoiceOver.TextToSpeech("Welcome TuDien");
        try{
            DictionaryManagement.InsertFromFile();
        } catch (IOException e){
            e.printStackTrace();
        }
        ChangeLang.setItems(setLanguage);
        ChangeLang.setValue("VI");
        Collections.sort(listWordTarget);
        ObservableList<String> data = FXCollections.observableArrayList(listWordTarget);
        listView.setItems(data);

    }

    //hàm chỉnh sửa nghĩa từ
    public void mod(KeyEvent event)
    {
        try {
            textArea.setEditable(true);
            if(event.getCode()==KeyCode.ENTER) {
                Alert alert=new Alert(Alert.AlertType.INFORMATION);
                alert.setTitle("Starting...");
                alert.setHeaderText("Thay đổi từ này");
                alert.showAndWait();
                alert.close();
                String modding=textArea.getText();
                String point=searchField.getText();
                String s=DictionaryManagement.modified(point, modding);
                textArea.setText(s);
                textArea.setEditable(false);
            }
        }catch (Exception e)
        {
            System.out.println("Thất bại. Thử lại");
        }

    }
}