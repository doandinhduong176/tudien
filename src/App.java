import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.Scanner;


public class App extends Application {    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        System.out.println("Chọn giao diện người dùng:\n" + "1: Giao diện tối.\n" + "2: Giao diện sáng");
        Scanner scanner = new Scanner(System.in);
        int choice = scanner.nextInt();
        if(choice == 1){
            Parent root = FXMLLoader.load(getClass().getResource("index_dark.fxml"));
            stage.setScene(new Scene(root));
            stage.setTitle("TuDien");
            stage.show();
        }   
        else {
            Parent root = FXMLLoader.load(getClass().getResource("index_shining.fxml"));
            stage.setScene(new Scene(root));
            stage.setTitle("TuDien");
            stage.show();
        }

           

        
    }
}