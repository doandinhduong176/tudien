package World;


public class Vocabulary {
    private final String english;
    private String vn;

    public Vocabulary() {
        this("", "");
    }

    public Vocabulary(String english, String vn) {
        this.english = english;
        this.vn = vn;
    }

    public String getEnglish() {
        return english;
    }

    public String getVn() {
        return vn;
    }

    public void setVn(String vn) {
        this.vn = vn;
    }

    public void print() {
        System.out.printf("| %-18s | %s", english, vn);
    }
}
