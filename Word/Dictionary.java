package World;

import java.util.ArrayList;

public class Dictionary {

    private static final ArrayList<Vocabulary> dictionary = new ArrayList<>();
    private static int index;//ký tự bắt đầu của 1 vocabulary

    public Dictionary() {
    }

    public ArrayList<Vocabulary> getDict() {
        return dictionary;
    }

    public void push(Vocabulary vocabulary) {
            dictionary.add(vocabulary);
    }

    public static Vocabulary binaryLookup(int l, int r, String vocabulary) {
        if (l >= r) {
            Vocabulary word = dictionary.get(l);
            if (word.getEnglish().startsWith(vocabulary)) {
                index = l;
                return dictionary.get(l);
            }
            return null;
        }

        int mid =(r + l) / 2;
        Vocabulary vocabulary1 = dictionary.get(mid);
        String currentVocabulary = vocabulary1.getEnglish();
        int compare = currentVocabulary.compareTo(vocabulary);
        if (compare == 0) {
            index = mid;
            return vocabulary1;
        }
        if (compare > 0) {
            return binaryLookup(l, mid, vocabulary);
        }
        return binaryLookup(mid + 1, r, vocabulary);
    }

    public void search(String english, ArrayList<Vocabulary> history, Dictionary dictionary) {
        Vocabulary search = binaryLookup(1, dictionary.getDict().size() - 1, english);
        if (search == null) {
            System.out.println("Không tìm thấy từ.");
        } else {
            System.out.println("|" + search.getEnglish() + "|");
            System.out.print(search.getVn());
            history.add(search);
        }
    }
    public static void dictionarySearcher(Dictionary dictionary, String vocabulay_eng) {
        for (Vocabulary v : dictionary.getDict()) {
            String v_english = v.getEnglish();
            String v_vn = v.getVn();
            if (v_english.startsWith(vocabulay_eng)) {
                System.out.printf("| %-18s |\n", v_english);
                System.out.println(v_vn);
            }
        }
    }

    public static int getIndex() {
        return index;// trả về giá trị index
    }
}
