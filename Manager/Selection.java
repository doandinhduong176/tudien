package Manager;

import java.util.Scanner;

/**
 * Kiểm tra đầu vào
 */
public class Selection {
    private static final Scanner sc = new Scanner(System.in);

    public static int limit(String choice, int min, int max) {
        System.out.print(choice);
        while (true) {
            int pick_number = Integer.parseInt(sc.nextLine().trim());
            if ((pick_number > max) || (pick_number < min)) {
                System.out.println("Chọn số trong khoảng từ " + min + " đến " + max + ".");
                System.out.print("Nhập lại: ");
            }
            return pick_number;
        }
    }

    public static String inputWord(String english, String turn) {
        while (true) {
            System.out.println(english);
            String vocabulary = sc.nextLine().trim();
            if (!vocabulary.matches(turn)) {
                System.out.println("Nhập từ.");
            } else {
                return vocabulary;
            }
        }
    }

    public static boolean Y_N(String english) {
        while (true) {
            String result = inputWord(english, ".+");

            if (result.equalsIgnoreCase("Y")) {
                return true;
            }
            if (result.equalsIgnoreCase("N")) {
                return false;
            }
            System.out.println("Chọn Yes or No.");
            System.out.print("Nhập lại: ");
        }
    }
}
