package Manager;

import java.io.IOException;
import java.util.ArrayList;

import World.Dictionary;
import World.Vocabulary;

public class DictionaryCommandline {

    public static final ArrayList<Vocabulary> history = new ArrayList<>();

    public static void showAllWords(Dictionary dictionary) {
        System.out.println("Note  | Tiếng anh            | Tiếng Việt");
        for (int i = 0; i < dictionary.getDict().size(); i++) {
            System.out.print(i + 1 + "   ");
            dictionary.getDict().get(i).print();
        }
    }

    public static <history> void dictionaryAdvanced(Dictionary dictionary, Object Input) throws IOException {
        DictionaryManagement.readFile("src\\dictionary.txt", dictionary);
        while (true) {
            System.out.println("1. Xem từ điển.");
            System.out.println("2. Xóa từ.");
            System.out.println("3. Tra từ.");
            System.out.println("4. Sửa.");
            System.out.println("5 Lịch sử.");
            System.out.println("6. Thoát.");
            int choice = Input.limit("Chọn thao tác: ", 1, 6);
            switch (choice) {
                case 1:
                    showAllWords(dictionary);
                    break;
                case 2:
                    DictionaryManagement.remove(dictionary, Input);
                    break;
                case 3:
                    String english = Input.inputWord("Nhập từ cần dịch: ", ".+");
                    dictionary.search(english, history, dictionary);
                    break;
                case 4:
                    DictionaryManagement.Fix(dictionary, Input);
                    break;
                case 5:
                    System.out.println("Lịch sử");
                    for (int i = 1; i < history.size() + 1; i++) {
                        System.out.println((i));
                        System.out.println(history.get(i).getEnglish());
                        System.out.println(history.get(i).getVn());
                        System.out.println();
                    }
                    break;
                case 6:
                    return;
            }
        }
    }
}
