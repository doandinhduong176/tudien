package Manager;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;

import World.Dictionary;
import World.Vocabulary;



public class DictionaryManagement {
    /**
     * Đọc file dictionary.txt
     */
    public static void readFile(String Name_file, Dictionary dictionary) {
        try {
            File f_new = new File(Name_file);
            insertFromFile(f_new, dictionary);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void insertFromFile(File file, Dictionary dictionary) throws IOException {
        FileReader fr = new FileReader(file);
        BufferedReader read = new BufferedReader(fr);
        String line;
        while ((line = read.readLine()) != null) {
            String[] Vocabularys = line.split("\\t+");
            String result = " ";
            for (int i = 1; i < Vocabularys.length; i++) {
                result += Vocabularys[i];
            }
            dictionary.push(new Vocabulary(Vocabularys[0], result));
        }
        read.close();
        fr.close();
    }

    /**
     * Tra từ
     */
    public static Vocabulary Search(String vocabulary, Dictionary dictionary) {
        for (Vocabulary vocabulary : dictionary.getDict()) {
            if (vocabulary.getEnglish().equalsIgnoreCase(vocabulary)) {
                return vocabulary;
            }
        }
        return null;
    }

    /**
     * thêm/ sửa từ
     * @param Input 
     */
    public static void Fix(Dictionary dictionary, Object Input) throws IOException {
        String english = Input.inputWord("Nhập từ tiếng anh cần thêm: ", ".+");
        Vocabulary search = Search(english, dictionary);
        if (search != null) {
            if (Input.Y_N("Cập nhật?")) {
                String vn = Input.inputWord("Xác nhận nghĩa mới của từ: ", ".+");
                search.setVn(vn);
            }
            dictionary.push(search);
            DictionaryCommandline.history.add(search);
            

        } else {
            String vn = Input.inputWord("Nhập nghĩa tiếng việt của từ vừa thêm: ", ".+");
            dictionary.push(new Vocabulary(english, vn));
            DictionaryCommandline.history.add(new Vocabulary(english, vn));
        }
    }

    /**
     * xóa từ
     * @param Input 
     */
    public static void remove(Dictionary dictionary, Object Input) throws IOException{
        String english = Input.inputWord("Chọn từ muốn xóa trong từ điển: ", ".+");
        Vocabulary search = Search(english, dictionary);
        if (search != null) {
            dictionary.getDict().remove(search);
            
        } else {
            System.out.println("Từ này không có trong từ điển.");
        }
    }
}